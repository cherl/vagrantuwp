﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HelixToolkit.Logger;
using HelixToolkit.UWP;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Vagrant_V1.Modeling3d._2D;
using Windows.UI.Xaml;

namespace Vagrant_V1
{
    public class MainPageViewModel : ObservableObject
    {
        public OITDemoViewModel OITVM { get; } = new OITDemoViewModel();

        private Vector3 upDirection = Vector3.UnitY;
        public Vector3 UpDirection
        {
            private set
            {
                if (Set(ref upDirection, value))
                {
                    ResetCamera();
                }
            }
            get { return upDirection; }
        }

        public IEffectsManager EffectsManager { private set; get; }
        public static Camera Camera { private set; get; }
        public TextureModel EnvironmentMap { private set; get; }
        public Vector3 DirectionalLightDirection { get; } = new Vector3(-0.5f, -1, 0);
        public Geometry3D Sphere { private set; get; }
        public Geometry3D Geometry { private set; get; }


        #region Commands
        #endregion

        public MainPageViewModel()
        {
            EffectsManager = new DefaultEffectsManager(new Logger());

            Camera = new OrthographicCamera()
            {
                Position = new Vector3(64, 133, 1487),
                LookDirection = new Vector3(0, -10, -100),
                UpDirection = UpDirection,
                FarPlaneDistance = 3000,
                NearPlaneDistance = 0.1,
                Width = 150
            };
            EnvironmentMap = LoadTexture("Cubemap_Grandcanyon.dds");
        }


        private Stream LoadTexture(string file)
        {
            var packageFolder = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "");
            var bytecode = global::SharpDX.IO.NativeFile.ReadAllBytes(packageFolder + @"\" + file);
            return new MemoryStream(bytecode);
        }

        public class Logger : ILogger
        {
            public void Log<MsgType>(LogLevel logLevel, MsgType msg, string className, string methodName, int lineNumber)
            {
                switch (logLevel)
                {
                    case LogLevel.Warning:
                    case LogLevel.Error:
                        Console.WriteLine($"Level:{logLevel}; Msg:{msg}");
                        break;
                }
            }
        }

        private void ResetCamera()
        {
            Camera.UpDirection = UpDirection;
            if (UpDirection == Vector3.UnitY || UpDirection == Vector3.UnitX)
            {
                Camera.Position = new Vector3(0, 0, -15);
                Camera.LookDirection = new Vector3(0, 0, 15);
            }
            else
            {
                Camera.Position = new Vector3(0, -15, 0);
                Camera.LookDirection = new Vector3(0, 15, 0);
            }
        }
    }
}
