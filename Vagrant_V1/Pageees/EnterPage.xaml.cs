﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class EnterPage : Page
    {
        public float loadingprogress = 0;

        public EnterPage()
        {
            this.InitializeComponent();
            Employer_View.GenerateUnityObjectAsync();
        }

        private void Enter(object sender, RoutedEventArgs e)
        {
            //var Start = new Employer_View();
            //LoadingControl.IsLoading = true;
            //Start.Progress_Load();
            Frame.Navigate(typeof(MainPage), "NOENABLE");
            //while(LoadingControl.IsLoading == true)
            //{
            //    if(loadingprogress >= 100)
            //    {
            //        LoadingControl.IsLoading = false;
            //    }
            //}
        }

        public async void Load_Start_Page()
        {
            //LoadingControl.IsLoading = false;
            Frame.Navigate(typeof(MainPage), "NOENABLE");
        }
    }
}
