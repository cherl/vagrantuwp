﻿using HelixToolkit.UWP;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Vagrant_V1.Data;
using Vagrant_V1.Pageees;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace Vagrant_V1
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private GeometryModel3D selectedElement = null;
        private DataGridDataSource viewModel1 = new DataGridDataSource();

        public MainPage()
        {
            this.InitializeComponent();
            OnXamlRendered();
        }



        //Работа с 3D
        private void Viewport3DX_OnMouse3DDown(object sender, HelixToolkit.UWP.MouseDown3DEventArgs e)
        {
            if (e.HitTestResult != null && e.HitTestResult.ModelHit is GeometryModel3D element)
            {
                if (selectedElement == element)
                {
                    selectedElement.PostEffects = null;
                    selectedElement = null;
                    return;
                }
                if (selectedElement != null)
                {
                    selectedElement.PostEffects = null;
                }
                selectedElement = element;
                if (selectedElement.Name != "floor")
                {
                    selectedElement.PostEffects = string.IsNullOrEmpty(selectedElement.PostEffects) ? "border[color:#00FFDE]" : null;
                }
            }
        }

        //!!!!!!!!!!!!!!!!!!!!!!!!
        public async void OnXamlRendered()
        {
            if (dataGrid != null)
            {
                dataGrid.Sorting -= DataGrid_Sorting;
                dataGrid.LoadingRowGroup -= DataGrid_LoadingRowGroup;
            }

            if (dataGrid != null)
            {
                dataGrid.Sorting += DataGrid_Sorting;
                dataGrid.LoadingRowGroup += DataGrid_LoadingRowGroup;
                dataGrid.ItemsSource = await viewModel1.GetDataAsync();

                var comboBoxColumn = dataGrid.Columns.FirstOrDefault(x => x.Tag.Equals("Id_Employer")) as DataGridComboBoxColumn;
                if (comboBoxColumn != null)
                {
                    comboBoxColumn.ItemsSource = await viewModel1.GetFIO();
                }

                var comboBoxColumnTag = dataGrid.Columns.FirstOrDefault(x => x.Tag.Equals("Tagg")) as DataGridComboBoxColumn;
                if (comboBoxColumnTag != null)
                {
                    comboBoxColumnTag.ItemsSource = await viewModel1.GetTag();
                }
            }

            //if (groupButton != null)
            //{
            //    groupButton.Click -= GroupButton_Click;
            //}

            //if (groupButton != null)
            //{
            //    groupButton.Click += GroupButton_Click;
            //}

            //if (rankLowItem != null)
            //{
            //    rankLowItem.Click -= RankLowItem_Click;
            //}

            ////rankLowItem = control.FindName("rankLow") as MenuFlyoutItem;
            //if (rankLowItem != null)
            //{
            //    rankLowItem.Click += RankLowItem_Click;
            //}

            //if (rankHighItem != null)
            //{
            //    rankHighItem.Click -= RankHigh_Click;
            //}

            ////rankHighItem = control.FindName("rankHigh") as MenuFlyoutItem;
            //if (rankHighItem != null)
            //{
            //    rankHighItem.Click += RankHigh_Click;
            //}

            //if (heightLowItem != null)
            //{
            //    heightLowItem.Click -= HeightLow_Click;
            //}

            ////heightLowItem = control.FindName("heightLow") as MenuFlyoutItem;
            //if (heightLowItem != null)
            //{
            //    heightLowItem.Click += HeightLow_Click;
            //}

            //if (heightHighItem != null)
            //{
            //    heightHighItem.Click -= HeightHigh_Click;
            //}

            ////heightHighItem = control.FindName("heightHigh") as MenuFlyoutItem;
            //if (heightHighItem != null)
            //{
            //    heightHighItem.Click += HeightHigh_Click;
            //}

            ////var clearFilter = control.FindName("clearFilter") as MenuFlyoutItem;
            //if (clearFilter != null)
            //{
            //    clearFilter.Click += this.ClearFilter_Click;
            //}
        }

        private void DataGrid_LoadingRowGroup(object sender, DataGridRowGroupHeaderEventArgs e)
        {
            ICollectionViewGroup group = e.RowGroupHeader.CollectionViewGroup;
            DataGridDataItem item = group.GroupItems[0] as DataGridDataItem;
            e.RowGroupHeader.PropertyValue = item.Team.ToString();
        }

        private void GroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.GroupData().View;
            }
        }

        private void DataGrid_Sorting(object sender, DataGridColumnEventArgs e)
        {
            // Clear previous sorted column if we start sorting a different column
            string previousSortedColumn = viewModel1.CachedSortedColumn;
            if (previousSortedColumn != string.Empty)
            {
                foreach (DataGridColumn dataGridColumn in dataGrid.Columns)
                {
                    if (dataGridColumn.Tag != null && dataGridColumn.Tag.ToString() == previousSortedColumn &&
                        (e.Column.Tag == null || previousSortedColumn != e.Column.Tag.ToString()))
                    {
                        dataGridColumn.SortDirection = null;
                    }
                }
            }

            // Toggle clicked column's sorting method
            if (e.Column.Tag != null)
            {
                if (e.Column.SortDirection == null)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), true);
                    e.Column.SortDirection = DataGridSortDirection.Ascending;
                }
                else if (e.Column.SortDirection == DataGridSortDirection.Ascending)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), false);
                    e.Column.SortDirection = DataGridSortDirection.Descending;
                }
                else
                {
                    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All);
                    e.Column.SortDirection = null;
                }
            }
        }

        private void RankLowItem_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Rank_Low);
            }
        }

        private void RankHigh_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Rank_High);
            }
        }

        private void HeightLow_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Height_Low);
            }
        }

        private void HeightHigh_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Height_High);
            }
        }

        private void ClearFilter_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All);
            }
        }

        private void Button_3D_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage), "ISENABLE");
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Administration_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ToggleSettings(object sender, RoutedEventArgs e)
        {

        }



        private void Info_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(EnterPage), "NOENABLE");
        }

    }
}
