﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Handlers;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.ModelData;
using Vagrant_V1.Pageees;
using Windows.UI.Xaml.Controls;

namespace Vagrant_V1.Server
{
    public class Employer_View
    {
        public static List<EmployerDataBase> EmployerDataBase;
        public static List<PositionDataBase> Position;
        public static List<ZonDataBase> Zon;
        public static List<TagData> Tag;
        public static List<UnityObject> unityObjects;

        //public Employer_View()
        //{
        //    var service = new ApiCrudService();
        //    tasks.Add(service.LoadAsync<ZonDataBase>("Zone"));

        //    var tasks = new List<Task>
        //    {
        //        new Task(_ => {Zon = service.LoadAsync<ZonDataBase>("Zone"); }),
        //        GenerateEmployersAsync(),
        //        GeneratePositionAsync(),
        //        GenerateZonAsync(),
        //        GenerateTagAsync()
        //    };
        //    Task.WaitAll(tasks.ToArray());
        //}

        public Employer_View()
        {


            //var tasks = new List<Task>
            //{

            //    GenerateEmployersAsync(),
            //    GeneratePositionAsync(),
            //    GenerateZonAsync(),
            //    GenerateTagAsync()
            //};
            //Task.WaitAll(tasks.ToArray());
            //Progress_Load();

        }

        // Получения сотрудников Get запрос
        public static async Task GenerateEmployersAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vagrantapi.azurewebsites.net/api/project/000T/Employee");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                EmployerDataBase = JsonConvert.DeserializeObject<List<EmployerDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        // Получения позицию сотрудника Get запрос
        public static async Task GeneratePositionAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vagrantapi.azurewebsites.net/api/project/000T/EmployeeZonePosition?simulate=true");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Position = JsonConvert.DeserializeObject<List<PositionDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        //// Получения зоны нахождения сотрудника Get запрос
        //public async Task<T> LoadAsync(string method) where T:object
        //{
        //    Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
        //    Uri requestUri = new Uri($"https://vagrantapi.azurewebsites.net/api/project/000T/{method}");
        //    Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
        //    string httpResponseBody = @"";
        //    try
        //    {
        //        httpResponse = await httpClient.GetAsync(requestUri);
        //        httpResponse.EnsureSuccessStatusCode();
        //        httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
        //        return JsonConvert.DeserializeObject<List<T>>(httpResponseBody);
        //    }
        //    catch (Exception ex)
        //    {
        //        httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
        //        throw;
        //    }
        //}

        // Получения зоны нахождения сотрудника Get запрос
        public static async Task GenerateZonAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vagrantapi.azurewebsites.net/api/project/000T/Zone");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Zon = JsonConvert.DeserializeObject<List<ZonDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }


        public static async Task GenerateUnityObjectAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vagrantapi.azurewebsites.net/api/project/000T/UnityObject?freeOnly=false");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                unityObjects = JsonConvert.DeserializeObject<List<UnityObject>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        // Получения Тэга  сотрудника Get запрос
        public static async Task GenerateTagAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vagrantapi.azurewebsites.net/api/project/000T/Tag?freeOnly=false");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Tag =  JsonConvert.DeserializeObject<List<TagData>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public async void Progress_Load()
        {
            await GenerateUnityObjectAsync();
            await GenerateEmployersAsync();
            await GeneratePositionAsync();
            await GenerateZonAsync();
            await GenerateTagAsync();
            var StartGame = new EnterPage();
            StartGame.Load_Start_Page();
        }
    }
}
