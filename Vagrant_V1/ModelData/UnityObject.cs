﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class UnityObjectList
    {
        public UnityObject[] Property1 { get; set; }
    }

    public class UnityObject
    {
        public string id { get; set; }
        public string project { get; set; }
        public string unityObjectId { get; set; }
        public Objectcoordinate[] objectCoordinates { get; set; }
        public Sitezone siteZone { get; set; }
    }

    public class Sitezone
    {
        public string id { get; set; }
        public string project { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string[] unityObjects { get; set; }
    }

    public class Objectcoordinate
    {
        public C1 c1 { get; set; }
        public C2 c2 { get; set; }
    }

    public class C1
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class C2
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }
}
