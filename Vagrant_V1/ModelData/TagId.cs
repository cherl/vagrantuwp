﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class TagId
    {
        public string id { get; set; }
        public string title { get; set; }
    }
}
