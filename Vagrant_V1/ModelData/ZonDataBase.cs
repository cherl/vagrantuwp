﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class ZonDataBaseList
    {
        public ZonDataBase[] Property1 { get; set; }
    }

    public class ZonDataBase
    {
        public string project { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string[] unityObjects { get; set; }
        public string id { get; set; }
    }
}
