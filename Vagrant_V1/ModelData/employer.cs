﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class Employer : INotifyPropertyChanged
    {

        private string fio;
        private string id_employer;
        private string position;
        private string unit;
        private string team;
        private InfoForPeople _infoforpeople;
        private TagId id_tag;


        public string FIO
        {
            get { return fio; }
            set { fio = value; OnPropertyChanged("Имя"); }
        }

        public string Id_Employer
        {
            get { return id_employer; }
            set { id_employer = value; OnPropertyChanged("Тэг"); }
        }

        public string Position
        {
            get { return position; }
            set { position = value; OnPropertyChanged("Должность"); }
        }

        public string Unit
        {
            get { return unit; }
            set { unit = value; OnPropertyChanged("Отдел"); }
        }

        public string Team
        {
            get { return team; }
            set { team = value; OnPropertyChanged("Бригада"); }
        }

        public TagId Id_Tag
        {
            get { return id_tag; }
            set
            {
                if (id_tag.title != null)
                    id_tag = value;
                else
                {
                    id_tag = value;
                    id_tag.title = "";
                }
            }
        }

        public InfoForPeople InfoForPeoples
        {
            get { return _infoforpeople; }
            set
            {
                _infoforpeople = value;
                OnPropertyChanged("Информация");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String name)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public Employer(string fio, string id_employer, string position, string unit, string team, InfoForPeople infoforpeople, TagId id_tag)
        {
            this.fio = fio;
            this.id_employer = id_employer;
            this.position = position;
            this.unit = unit;
            this.team = team;
            this._infoforpeople = infoforpeople;
            this.id_tag = id_tag;
        }
    }


    public class InfoForPeople : INotifyPropertyChanged
    {
        private string _fio;
        private string _zone;
        private int _time_on_zon;

        public string FIO
        {
            get { return _fio; }
            set
            {
                _fio = value;
                OnPropertyChanged("Имя");
            }
        }

        public string Zone
        {
            get { return _zone; }
            set
            {
                _zone = value;
                OnPropertyChanged("Зона");
            }
        }

        public int Time
        {
            get { return _time_on_zon; }
            set
            {
                _time_on_zon = value;
                OnPropertyChanged("Время");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String name)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        public InfoForPeople(string fio, string zone, int time)
        {
            this._fio = fio;
            this._zone = zone;
            this._time_on_zon = time;
        }
    }
}
