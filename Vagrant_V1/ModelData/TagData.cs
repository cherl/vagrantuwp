﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class TagDataList
    {
        public TagData[] Property1 { get; set; }
    }

    public class TagData
    {
        public string title { get; set; }
        public string project { get; set; }
        public string employee { get; set; }
        public string id { get; set; }
    }
}
