﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class EmployerDataBaseList
    {
        public EmployerDataBase[] Property1 { get; set; }
    }

    public class EmployerDataBase
    {
        public string id { get; set; }
        public string externalId { get; set; }
        public string title { get; set; }
        public string position { get; set; }
        public TagId tag { get; set; }
        public string accessLevel { get; set; }
        public string unit { get; set; }
        public string team { get; set; }
    }
}
