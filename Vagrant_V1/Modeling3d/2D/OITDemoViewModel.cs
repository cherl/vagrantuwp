﻿using GalaSoft.MvvmLight;
using HelixToolkit.UWP;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vagrant_V1.Server;

namespace Vagrant_V1.Modeling3d._2D
{
    public class OITModel : ObservableObject
    {
        public Geometry3D Model { private set; get; }

        public Material Material { private set; get; }

        public bool IsTransparent { private set; get; }

        private bool showWireframe = false;

        public bool ShowWireframe
        {
            set
            {
                Set(ref showWireframe, value);
            }
            get
            {
                return showWireframe;
            }
        }

        public OITModel(Geometry3D model, Material material, bool isTransparent)
        {
            Model = model;
            Material = material;
            IsTransparent = isTransparent;
        }
    }

    public class OITDemoViewModel : ObservableObject
    {
        public ObservableCollection<OITModel> ModelGeometry { get; private set; } = new ObservableCollection<OITModel>();

        public Matrix Transform { private set; get; } = Matrix.Translation(60, -10, 0);
        public LineGeometry3D GridModel { private set; get; }
        public Matrix GridTransform { private set; get; } = Matrix.Translation(60, -10, 0);
        public OITWeightMode[] OITWeights { get; } = new OITWeightMode[] { OITWeightMode.Linear0, OITWeightMode.Linear1, OITWeightMode.Linear2, OITWeightMode.NonLinear };

        private bool showWireframe = false;

        public bool ShowWireframe
        {
            set
            {
                if (Set(ref showWireframe, value))
                {
                    foreach (var item in ModelGeometry)
                    {
                        item.ShowWireframe = value;
                    }
                }
            }
            get
            {
                return showWireframe;
            }
        }

        private SynchronizationContext syncContext = SynchronizationContext.Current;

        public OITDemoViewModel()
        {
            var packageFolder = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "Taishet_Clear.stl");
            BuildGrid();
            Task.Run(() => { LoadStl(packageFolder); });
        }
        private void BuildGrid()
        {
            var builder = new LineBuilder();
            int zOff = -45;
            for (int i = 0; i < 10; ++i)
            {
                for (int j = 0; j < 10; ++j)
                {
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, -j * 5));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(-i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, j * 5, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, zOff), new SharpDX.Vector3(-i * 5, j * 5, zOff));
                }
            }
            GridModel = builder.ToLineGeometry3D();
        }

        public void Load3ds(string path)
        {
            var reader = new StudioReader();
            var objCol = reader.Read(path);
            AttachModelList(objCol);
        }

        public void LoadObj(string path)
        {
            var reader = new ObjReader();
            var objCol = reader.Read(path);
            AttachModelList(objCol);
        }

        public void LoadStl(string path)
        {
            var reader = new StLReader();
            var objCol = reader.Read(path);
            AttachModelList(objCol);
        }


        public void AttachModelList(List<Object3D> objs)
        {

            Random rnd = new Random(DateTime.Now.Millisecond);

            foreach (var ob in objs)
            {
                ob.Geometry.UpdateOctree();
                Task.Delay(100).Wait();
                syncContext.Post(
                    (o) =>
                    {
                        if (ob.Material is HelixToolkit.UWP.Model.PhongMaterialCore p)
                        {
                            var diffuse = p.DiffuseColor;
                            diffuse.Red = (float)0.6760413;
                            diffuse.Green = (float)0.6760413;
                            diffuse.Blue = (float)0.6760413;
                            diffuse.Alpha = 0.5f;//(float)(Math.Min(0.8, Math.Max(0.2, rnd.NextDouble())));
                            p.DiffuseColor = diffuse;
                            this.ModelGeometry.Add(new OITModel(ob.Geometry, p, p.DiffuseColor.Alpha < 0.9));
                            GroupModel3D Sphere1 = new GroupModel3D();
                            DynamicReflectionMap3D Sphere1dynamic = new DynamicReflectionMap3D();
                            Sphere1dynamic.IsDynamicScene = true;
                            for (int i = 0; i < Employer_View.unityObjects.Count; i++)
                            {
                                MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                                Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                                var builder = new MeshBuilder(true, true, true);
                                builder = new MeshBuilder();
                                for (int j = 0; j < Employer_View.unityObjects[i].objectCoordinates.Length; j++ )
                                {
                                    Vector3 centre = ob.Geometry.Bound.Center;
                                    centre.X = centre.X + ((Employer_View.unityObjects[i].objectCoordinates[j].c1.x + Employer_View.unityObjects[i].objectCoordinates[j].c2.x) / 2);
                                    centre.Y = centre.Y - ((Employer_View.unityObjects[i].objectCoordinates[j].c1.y + Employer_View.unityObjects[i].objectCoordinates[j].c2.y) / 2);
                                    centre.Z = centre.Z + ((Employer_View.unityObjects[i].objectCoordinates[j].c1.z + Employer_View.unityObjects[i].objectCoordinates[j].c2.z) / 2);
                                    builder.AddBox(centre,Math.Abs(Employer_View.unityObjects[i].objectCoordinates[j].c1.x - Employer_View.unityObjects[i].objectCoordinates[j].c2.x)
                                        , Math.Abs(Employer_View.unityObjects[i].objectCoordinates[j].c1.y - Employer_View.unityObjects[i].objectCoordinates[j].c1.y), 1);
                                    Geometry3D SphereGeometry;
                                    SphereGeometry = builder.ToMesh();
                                    SphereGeometry.UpdateOctree();
                                    Spheremodel.Geometry = SphereGeometry;
                                    Spheremodel.IsThrowingShadow = true;
                                    PhongMaterial Material1;
                                    Material1 = new PhongMaterial()
                                    {
                                        AmbientColor = Color.Gray,
                                        DiffuseColor = new Color4(0.75f, 0.75f, 0.75f, 1.0f),
                                        SpecularColor = Color.White,
                                        SpecularShininess = 10f,
                                        ReflectiveColor = new Color4(0.2f, 0.2f, 0.2f, 0.5f),
                                        RenderEnvironmentMap = true
                                    };
                                    Material1.ReflectiveColor = Color.Silver;
                                    Material1.RenderDiffuseMap = false;
                                    Material1.RenderNormalMap = false;
                                    Material1.RenderEnvironmentMap = true;
                                    Material1.RenderShadowMap = true;
                                    Spheremodel.Material = Material1;
                                    Sphere1dynamic.Children.Add(Spheremodel);
                                    Sphere1.Children.Add(Sphere1dynamic);
                                    this.ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true));
                                }
                            }
                        }
                    }, null);
            }
        }
    }
}
