﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.ModelData;

namespace Vagrant_V1.Data
{
        public class DataGridDataItem : INotifyDataErrorInfo, IComparable
        {
            private Dictionary<string, List<string>> _errors = new Dictionary<string, List<string>>();
            private string fio;
            private string id_employer;
            private string position;
            private string unit;
            private uint team;
            private string tagg;
            private InfoForPeople _infoforpeople;

            public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

            public string Tagg
            {
                get { return tagg; }
                set
                {
                    if (tagg != value)
                    {
                        tagg = value;
                    }
                }
            }

            public string FIO
            {
                get { return fio; }
                set
                {
                    if (fio != value)
                    {
                        fio = value;
                        bool isFioValid = !_errors.Keys.Contains("FIO");
                        if (fio == string.Empty && isFioValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("FIO name cannot be empty");
                            _errors.Add("FIO", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("FIO"));
                        }
                        else if (fio != string.Empty && !isFioValid)
                        {
                            _errors.Remove("FIO");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("FIO"));
                        }
                    }
                }
            }

            public string Id_Employer
            {
                get { return id_employer; }
                set
                {
                    if (id_employer != value)
                    {
                        id_employer = value;
                        bool isid_employerValid = !_errors.Keys.Contains("Tag");
                        if (id_employer == string.Empty && isid_employerValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Tag name cannot be empty");
                            _errors.Add("Tag", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Tag"));
                        }
                        else if (id_employer != string.Empty && !isid_employerValid)
                        {
                            _errors.Remove("Tag");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Tag"));
                        }
                    }
                }
            }

            public string Position
            {
                get { return position; }
                set
                {
                    if (position != value)
                    {
                        position = value;
                        bool ispositionValid = !_errors.Keys.Contains("Position");
                        if (position == string.Empty && ispositionValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Position name cannot be empty");
                            _errors.Add("Position", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Position"));
                        }
                        else if (position != string.Empty && !ispositionValid)
                        {
                            _errors.Remove("Position");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Position"));
                        }
                    }
                }
            }

            public string Unit
            {
                get { return unit; }
                set
                {
                    if (unit != value)
                    {
                        unit = value;
                        bool isunitValid = !_errors.Keys.Contains("Unit");
                        if (unit == string.Empty && isunitValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Unit name cannot be empty");
                            _errors.Add("Unit", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Unit"));
                        }
                        else if (unit != string.Empty && !isunitValid)
                        {
                            _errors.Remove("Unit");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Unit"));
                        }
                    }
                }
            }

            public uint Team
            {
                get { return team; }
                set
                {
                    if (team != value)
                    {
                        team = value;
                    }
                }
            }


            public string Name { get; set; }

            public uint Time { get; set; }

            public uint Number { get; set; }

            public string Zon { get; set; }

            bool INotifyDataErrorInfo.HasErrors
            {
                get
                {
                    return _errors.Keys.Count > 0;
                }
            }

            IEnumerable INotifyDataErrorInfo.GetErrors(string propertyName)
            {
                if (propertyName == null)
                {
                    propertyName = string.Empty;
                }

                if (_errors.Keys.Contains(propertyName))
                {
                    return _errors[propertyName];
                }
                else
                {
                    return null;
                }
            }

            int IComparable.CompareTo(object obj)
            {
                int lnCompare = team.CompareTo((obj as DataGridDataItem).team);

                if (lnCompare == 0)
                {
                    return fio.CompareTo((obj as DataGridDataItem).fio);
                }
                else
                {
                    return lnCompare;
                }
            }
        }

}
