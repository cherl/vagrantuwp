﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.Server;
using Windows.UI.Xaml.Data;

namespace Vagrant_V1.Data
{
    [Bindable]
    public class DataGridDataSource
    {
        private static ObservableCollection<DataGridDataItem> _items;
        private static List<string> id_employer;
        private static List<string> Tag_all;
        private static CollectionViewSource groupedItems;
        private string _cachedSortedColumn = string.Empty;

        // Loading data
        public async Task<IEnumerable<DataGridDataItem>> GetDataAsync()
        {
            await Employer_View.GenerateEmployersAsync();
            await Employer_View.GeneratePositionAsync();
            await Employer_View.GenerateZonAsync();
            await Employer_View.GenerateTagAsync();
            _items = new ObservableCollection<DataGridDataItem>(
            Employer_View
                .EmployerDataBase
                .Select(v => new DataGridDataItem()
                {
                    FIO = v.title,
                    Id_Employer = v.id,
                    Position = v.position,
                    Team = uint.Parse(v.team),
                    Unit = v.unit,
                    Name = v.title,
                    Tagg = v.tag == null ? "" : v.tag.title,
                    Time = 0,
                    Number = 10000,
                    Zon = Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId).description
                }));

            //foreach (Vagrant.UWP.Model_data.Class1 v in Employer_View.EmployerDataBase)
            //{
            //   // string tagnew = Employer_View.EmployerDataBase[i].tag == null ? "" : Employer_View.EmployerDataBase[i].tag.title;
            //    _items.Add(new DataGridDataItem()
            //    {
            //        FIO = v.title,
            //        Id_Employer = v.id,
            //        Position = v.position,
            //        Team = uint.Parse(v.team),
            //        Unit = v.unit,
            //        Name = v.title,
            //        Tagg = v.tag == null ? "" : v.tag.title,
            //        Time = 0,
            //        Number = 10000,
            //        Zon = "Привет"
            //    });
            //}
            return _items;
        }

        // Load mountains into separate collection for use in combobox column
        public async Task<IEnumerable<string>> GetFIO()
        {
            if (_items == null || !_items.Any())
            {
                await GetDataAsync();
            }

            id_employer = _items?.OrderBy(x => x.Id_Employer).Select(x => x.Id_Employer).Distinct().ToList();

            return id_employer;
        }

        public async Task<IEnumerable<string>> GetTag()
        {
            if (Employer_View.Tag == null || !Employer_View.Tag.Any())
            {
                await GetDataAsync();
            }

            Tag_all = Employer_View.Tag?.OrderBy(x => x.title).Select(x => x.title).Distinct().ToList();

            return Tag_all;
        }

        // Sorting implementation using LINQ
        public string CachedSortedColumn
        {
            get
            {
                return _cachedSortedColumn;
            }

            set
            {
                _cachedSortedColumn = value;
            }
        }

        public ObservableCollection<DataGridDataItem> SortData(string sortBy, bool ascending)
        {
            _cachedSortedColumn = sortBy;
            switch (sortBy)
            {
                case "FIO":
                    return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.FIO))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.FIO));

                //if (ascending)
                //{
                //    //return new ObservableCollection<DataGridDataItem>(from item in _items
                //    //                                                  orderby item.FIO ascending
                //    //                                                  select item);
                //}
                //else
                //{
                //    //return new ObservableCollection<DataGridDataItem>(from item in _items
                //    //                                                  orderby item.FIO descending
                //    //                                                  select item);
                //}

                case "Id_Employer":
                    if (ascending)
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Id_Employer ascending
                                                                          select item);
                    }
                    else
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Id_Employer descending
                                                                          select item);
                    }

                case "Position":
                    if (ascending)
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Position ascending
                                                                          select item);
                    }
                    else
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Position descending
                                                                          select item);
                    }

                case "Unit":
                    if (ascending)
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Unit ascending
                                                                          select item);
                    }
                    else
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Unit descending
                                                                          select item);
                    }

                case "Team":
                    if (ascending)
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Team ascending
                                                                          select item);
                    }
                    else
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Team descending
                                                                          select item);
                    }
                case "Tagg":
                    if (ascending)
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Tagg ascending
                                                                          select item);
                    }
                    else
                    {
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Tagg descending
                                                                          select item);
                    }
            }

            return _items;
        }

        // Grouping implementation using LINQ
        public CollectionViewSource GroupData()
        {
            ObservableCollection<GroupInfoCollection<DataGridDataItem>> groups = new ObservableCollection<GroupInfoCollection<DataGridDataItem>>();

            //_items.


            var query = from item in _items
                        orderby item
                        group item by item.Team into g
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>
                {
                    Key = g.GroupName
                };
                g.Items.ToList().ForEach(item => info.Add(item));
                groups.Add(info);
            }


            //foreach (var g in query)
            //{
            //    GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>();
            //    info.Key = g.GroupName;
            //    foreach (var item in g.Items)
            //    {
            //        info.Add(item);
            //    }

            //    groups.Add(info);
            //}

            groupedItems = new CollectionViewSource();
            groupedItems.IsSourceGrouped = true;
            groupedItems.Source = groups;

            return groupedItems;
        }

        public class GroupInfoCollection<T> : ObservableCollection<T>
        {
            public object Key { get; set; }

            public new IEnumerator<T> GetEnumerator()
            {
                return (IEnumerator<T>)base.GetEnumerator();
            }
        }

        // Filtering implementation using LINQ
        public enum FilterOptions
        {
            All = -1,
            Rank_Low = 0,
            Rank_High = 1,
            Height_Low = 2,
            Height_High = 3
        }

        public ObservableCollection<DataGridDataItem> FilterData(FilterOptions filterBy)
        {
            switch (filterBy)
            {
                case FilterOptions.All:
                    return new ObservableCollection<DataGridDataItem>(_items);

                case FilterOptions.Rank_Low:
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Team == 1
                                                                      select item);

                case FilterOptions.Rank_High:
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Team > 1
                                                                      select item);

                case FilterOptions.Height_High:
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Team < 3
                                                                      select item);

                case FilterOptions.Height_Low:
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Team == 4
                                                                      select item);
            }

            return _items;
        }
    }
}
